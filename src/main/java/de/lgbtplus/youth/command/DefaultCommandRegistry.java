package de.lgbtplus.youth.command;

import de.lgbtplus.youth.Youth;
import de.lgbtplus.youth.misc.YouthUser;
import lombok.Getter;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DefaultCommandRegistry extends ListenerAdapter implements CommandRegistry {

  @Getter
  private final String PREFIX = ".";
  @Getter
  private List<Command> commands = new ArrayList<>();

  @Override
  public boolean registerCommand(Command command) {
    return this.commands.add(command);
  }

  @Override
  public boolean unregisterCommand(Command command) {
    return this.commands.remove(command);
  }

  @Override
  public void onMessageReceived(MessageReceivedEvent event) {
    if (event.getAuthor().isBot()) return;
    if (!event.isFromType(ChannelType.TEXT)) return;

    Message message = event.getMessage();
    String content = message.getContentRaw().trim();
    String[] splitted = content.contains(" ") ? content.split(" ") : new String[]{content};
    if (!content.startsWith(PREFIX)) return;

    String commandName = splitted[0].substring(PREFIX.length(), splitted[0].length());
    String[] args = Arrays.copyOfRange(splitted, 1, splitted.length);
    Command command = getCommandObjectByName(commandName);

    YouthUser youthUser = Youth.getInstance().getUserManager().getYouthUser(event.getAuthor().getName(), event.getAuthor().getDiscriminator());
    if(!commandName.equalsIgnoreCase("dsgvo")) {
      if (youthUser == null) {
        event.getMessage().getChannel().sendMessage("Bei der Benutzung von Commands werden personenbezogene Daten von dir gespeichert. Wenn du dem zustimmst, gib bitte '.dsgvo accept' ein.").queue();
        return;
      }
      if(!youthUser.isAccepted()) {
        event.getMessage().getChannel().sendMessage("Bei der Benutzung von Commands werden personenbezogene Daten von dir gespeichert. Wenn du dem zustimmst, gib bitte '.dsgvo accept' ein.").queue();
        return;
      }
    }

    if (command == null) {
      message.getChannel().sendMessage("Command not found.").queue();
      return;
    }
    CommandResponse commandResponse = command.triggerCommand(message, args);
  }

  @Override
  public Command getCommandObjectByClass(Class<? extends Command> commandClass) {
    return this.commands.stream().filter(command -> command.getClass().equals(commandClass)).findFirst().orElse(null);
  }

  @Override
  public Command getCommandObjectByName(String commandName) {
    return this.commands.stream()
            .filter(command -> command.getCommandName().equalsIgnoreCase(commandName))
            .findFirst()
            .orElse(null);
  }

}
