package de.lgbtplus.youth.command.commands;

import de.lgbtplus.youth.command.Command;
import de.lgbtplus.youth.command.CommandResponse;
import de.lgbtplus.youth.misc.Messages;
import de.lgbtplus.youth.utils.Emotes;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageReaction;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SurveyCommand extends Command {


    public SurveyCommand() { super("survey", "\"<question>\" [vote emotes]", "Ask a (yes or no) question"); }

    private final Pattern questionPattern = Pattern.compile("\"(.*?)\"");

    private String question;
    private Matcher matcher;

    @Override
    public CommandResponse triggerCommand(Message message, String[] args) {
        if (args.length == 0) {
            message.getChannel().sendMessage(String.format(Messages.USAGE, "." + getCommandName() + " " + getSyntax())).queue();
            return CommandResponse.ACCEPTED;
        }

        this.matcher = this.questionPattern.matcher(message.getContentRaw());

        if(matcher.find()) {
            this.question = this.matcher.group(1);
        } else {
            message.getChannel().sendMessage("Bitte gib eine Frage in Anführungszeichen ein.").queue();
            return CommandResponse.ACCEPTED;
        }

        message.delete().queue();

        EmbedBuilder embed = new EmbedBuilder()
                .setColor(0xc23616)
                .setAuthor("— Neue Umfrage —", "https://lgbtplus.de/t/lgbtplusde", "https://data.lgbtplus.de/branding/TwitterDesign/Profile_Picture_Colorful.png")
                .addField("Benutzer", "" + message.getAuthor().getAsMention(), false)
                .addField("Frage", this.question, false);

        if (message.getContentRaw().trim().charAt(message.getContentRaw().length() - 1) == '"') {
            message.getChannel().sendMessage(embed.build()).queue(sentMessage -> {
                sentMessage.addReaction("\uD83D\uDC4D").queue();
                sentMessage.addReaction("\uD83D\uDC4E").queue();
            });
            return CommandResponse.ACCEPTED;
        }
        String arguments = message.getContentRaw().replace(".survey ", "").replace(question, "").replace("\"", "").trim();
        message.getChannel().sendMessage(embed.build()).queue(sentMessage -> {
            for (String emoteString : arguments.split(" ")) {
                sentMessage.addReaction(emoteString).queue();
            }
        });

        return CommandResponse.ACCEPTED;
    }
}