package de.lgbtplus.youth.command.commands;

import de.lgbtplus.youth.Youth;
import de.lgbtplus.youth.command.Command;
import de.lgbtplus.youth.command.CommandResponse;
import de.lgbtplus.youth.misc.Messages;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;

import java.time.LocalDateTime;
import java.time.ZoneId;

public class RulesCommand extends Command {

    public RulesCommand() {
        super("rules", "<1|2>", "Print the rules.");
    }

    @Override
    public CommandResponse triggerCommand(Message message, String[] args) {
        if (args.length == 0) {
            if (!Youth.getInstance().hasRole(message.getMember(), ".")) {
                message.getChannel().sendMessage(String.format(Messages.NOPERM, message.getMember().getAsMention())).queue();
                return CommandResponse.ACCEPTED;
            }
            message.getChannel().sendMessage(String.format(Messages.USAGE, "." + getCommandName() + " " + getSyntax())).queue();
            return CommandResponse.ACCEPTED;
        } 
        if (Youth.getInstance().hasRole(message.getMember(), ".")) {
            switch (args[0]) {
                case "2": {
                    EmbedBuilder embed = new EmbedBuilder()
                            .setAuthor("Regelwerk", "https://lgbtplus.de/t/lgbtplusde", "https://data.lgbtplus.de/branding/TwitterDesign/Profile_Picture_Colorful.png")
                            .setColor(0x192a56)
                            .addField("Sei fair!", "Gib Mobbing oder anderen Diskriminierungen keine Chance! Behandle alle Mitglieder so, wie du auch behandelt werden möchtest. Jeder hat seine eigene Individualität, sollte dir auffallen das ein Mitglied gemobbt wird oder ähnliches, melde dies bitte umgehend einem Teammitglied.", false)
                            .addBlankField(true)
                            .addField("Sei respektvoll!", "Respektiere die Anderen! Du musst jemanden nicht mögen oder einer Meinung mit ihm sein, aber du musst sie und ihre jeweilige Meinung respektieren. ", false)
                            .addBlankField(true)
                            .addField("Sei einzigartig!", "Rassismus und Ausgrenzung haben bei uns keinen Platz! Jeder ist auf seine Art und Weise einzigartig und in unserer Community somit herzlich eingeladen.", false)
                            .addBlankField(true)
                            .addField("Sei offen!", "Finde neue Freunde, Gleichgesinnte und führe interessante Konversationen mit allen möglichen Menschen, ob aus deiner Umgebung oder aus ganz Deutschland!", false)
                            .addBlankField(true)
                            .addField("Sei anständig!", "Pornographische Inhalte und anzügliche Kommentare gehören hier nicht her! Bleib vernünftig und genieße die Zeit auf unserer Plattform. ", false)
                            .addBlankField(true)
                            .addField("Sei frei!", "Fühle dich frei deine Meinung zu äußern! Jeder hat seine eigene Meinung. Bleib aber bitte immer sachlich und nutze keine diskriminierende Wörter!", false)
                            .addBlankField(true)
                            .addField("Sei nicht aufdringlich!", "Zwänge keinem anderen YouthUser deine Discords, Server oder Produkte auf. Unser Discord soll ein werbefreier Raum bleiben und sich nicht in einen Marktplatz entwickeln. Bitte verzichte deshalb auf jegliche (Schleich-) Werbung", false)
                            .addBlankField(true)
                            .addField("Hilfe", "Solltest du dich bedrängt, gemobbt, ungerecht behandelt fühlen oder einfach nur eine Frage an uns haben, dann zögere nicht einen der Administratoren oder Supporter anzusprechen. Wir helfen Dir gerne bestmöglich weiter.", false)
                            .setFooter("Aktuelle Regeln vom", "https://data.lgbtplus.de/branding/Transparents/logo-color.svg").setTimestamp(LocalDateTime.now(ZoneId.of("Europe/Berlin")));
                    message.delete().queue();
                    message.getChannel().sendMessage(embed.build()).queue();
                    return CommandResponse.ACCEPTED;
                }
                case "1": {
                    EmbedBuilder embed = new EmbedBuilder()
                            .setAuthor("Regelwerk", "https://lgbtplus.de/t/lgbtplusde", "https://data.lgbtplus.de/branding/TwitterDesign/Profile_Picture_Colorful.png")
                            .setColor(0x192a56)
                            .addField("§1 Regelwerk & Geltungsbereich", "1. Dieses Regelwerk hat auf dem ganzen Discord, sowie alle \n   dazugehörigen Komponenten seine Gültigkeit \n2. Ein Teammitglied ist nicht verpflichtet bei nichtbeachtung\n   der Regeln einen Grund zu nennen.\n3. Unwissenheit schützt vor Strafe nicht. \n4. Ein Teammitglied verteilt Strafen Fair, aber nach eigenem\n   Ermessen. \n5. Am Regelwerk können jederzeit Änderungen vorgenommen \n   werden. Daher solltest Du dich regelmäßig über den \n   aktuellen Stand der Regeln in Kenntnis setzen.", true)
                            .addBlankField(true)
                            .addBlankField(true)
                            .addField("§2 Milderung von Strafen", "1. Du bist berechtigt einen Entbannungsantrag zu stellen, \n   wenn du die einen Bann als ungerecht empfindest.\n2. Du darfst maximal einen Entbannungsantrag alle 30 Tage\n   stellen.\n3. Ein Entbannungsantrag kann unter\n   entbannung@lgbtplus.de eingereicht werden.\n4. Mögliche Milderungen sind:\n    - Verkürzung des Bannes\n    - Sofortige Entbannung", true)
                            .addBlankField(true)
                            .addBlankField(true)
                            .addField("§3 Allgemeine Regelungen", "1. Rechtswidrige, rassistische, sexistische, gewalttätige, politisch oder religiös extremistische, diskriminierende oder andere regelwidrige Äußerungen zu tätigen sowie Fotos, die andere Personen verletzen, zu veröffentlichen ist verboten.\n2. Dazu gehören ebenso:\n    - Nacktbilder\n    - pornografische oder gewaltverherrlichende Darstellungen \n      in Schrift und Bild\n    - Symbole sowie Abbildungen von Opfern eines \n      Gewaltverbrechens\n    - Links zu Internetseiten, die Teile der genannten Inhalte \n      darstellen\n3. Ein respektvoller Umgang mit jedem YouthUser ist verpflichtend.\n4. Damit sind:\n    - Beleidigung\n    - Beschimpfung\n    - Mobbing bzw. Cyberbullying\n    - Stalking\n    - Spam \noder sonstige Kommentare, die dazu geeignet sind einer anderen Person zu schaden ausgeschlossen.", true)
                            .addBlankField(true)
                            .addBlankField(true)
                            .addField("§4 Sonstige Regelungen", "1. Den Anweisungen des Serverteams ist Folge zu leisten.\n2. Das Markieren von Gruppen ist nicht erlaubt und sollte nur in Ausnahmefällen benutzt werden.\n3. Das aktive und passive Werben für andere Discords, Server, Websites, Produkte, etc. Ist grundsätzlich ohne das Einverständnis des Serverteams untersagt.\n4. Diese Regeln gelten in allen Text- und Sprach-Kanälen sowie in der Statusmeldung unter dem Namen.", true)
                            .setFooter("Aktuelle Regeln vom", "https://data.lgbtplus.de/branding/Transparents/logo-color.svg").setTimestamp(LocalDateTime.now(ZoneId.of("Europe/Berlin")));
                    message.delete().queue();
                    message.getChannel().sendMessage(embed.build()).queue();
                    return CommandResponse.ACCEPTED;
                }
            }
        }
        return CommandResponse.ACCEPTED;
    }
}
