package de.lgbtplus.youth.command.commands;

import de.lgbtplus.youth.Youth;
import de.lgbtplus.youth.command.Command;
import de.lgbtplus.youth.command.CommandResponse;
import de.lgbtplus.youth.misc.Messages;
import de.lgbtplus.youth.utils.Emotes;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.entities.Role;

import java.util.ArrayList;
import java.util.List;

public class RoleCommand extends Command {

    private MessageEmbed helpMessage;

    public RoleCommand() {
        super("role", "<group>", "Give yourself a role");
    }

    @Override
    public CommandResponse triggerCommand(Message message, String[] args) {

        if (args.length == 0) {
            //Embed Message
            EmbedBuilder embed = new EmbedBuilder();
            embed.setAuthor("Themen", "https://lgbtplus.de/t/lgbtplusde", "https://data.lgbtplus.de/branding/TwitterDesign/Profile_Picture_Colorful.png");
            embed.setColor(2003199);

            embed.addField("**Benutze den jeweiligen Befehl:**", ".", false);
            embed.addField("Alle Kanäle", "**#all** \n .role all", true);
            embed.addField(":warning:", "**#nsfw** \n .role nsfw", true);
            embed.addField(":film_frames:", "**#musik** \n .role musik-und-videos", true);
            embed.addField(":speech_balloon:", "**#spam** \n .role spam", true);
            embed.addField(":musical_note:", "**#musik-commands** \n .role musik-cmd", true);
            embed.addField(":video_game:", "**#gaming** \n .role gaming", true);
            embed.addField(new Emotes("trump").toEmote().getAsMention(), "**#politics** \n .role politik", true);
            embed.addField(new Emotes("cog").toEmote().getAsMention(), "**#technik-und-entwicklung** \n .role technik", true);
            embed.addField(new Emotes("netflixdate").toEmote().getAsMention(), "**#serien** \n .role serien", true);
            embed.addField(":gay_pride_flag:", "**#lgbt+** \n .role lgbt", true);
            embed.addField(new Emotes("photoshop").toEmote().getAsMention(), "**#design-und-kreatives** \n .role design", true);
            embed.addField(new Emotes("merkel").toEmote().getAsMention(), "**#selfies** \n .role selfies", true);
            embed.addField(new Emotes("meme").toEmote().getAsMention(), "**#memes** \n .role memes", true);
            embed.addField(":warning:", "**#kinkynsfw** \n .role kinkynsfw", true);

            helpMessage = embed.build();
            message.getChannel().sendMessage(helpMessage).queue();
            return CommandResponse.ACCEPTED;
        } else {

            Role nsfwRole = message.getGuild().getRolesByName("nsfw", true).get(0);
            Role lgbtRole = message.getGuild().getRolesByName("LGBT+", true).get(0);
            Role musicRole = message.getGuild().getRolesByName("musik-und-videos", true).get(0);
            Role spamRole = message.getGuild().getRolesByName("spam", true).get(0);
            Role gamingRole = message.getGuild().getRolesByName("gaming", true).get(0);
            Role musicCMDRole = message.getGuild().getRolesByName("musik-cmd", true).get(0);
            Role politicsRole = message.getGuild().getRolesByName("politics", true).get(0);
            Role technikRole = message.getGuild().getRolesByName("technik", true).get(0);
            Role serienRole = message.getGuild().getRolesByName("serien", true).get(0);
            Role creativeRole = message.getGuild().getRolesByName("creative", true).get(0);
            Role selfiesRole = message.getGuild().getRolesByName("selfies", true).get(0);
            Role memesRole = message.getGuild().getRolesByName("memes", true).get(0);
            Role kinkRole = message.getGuild().getRolesByName("kinkynsfw", true).get(0);
            Role allRole = message.getGuild().getRolesByName("all", true).get(0);

            List<Role> allRoles = new ArrayList<>();
            allRoles.add(nsfwRole);
            allRoles.add(lgbtRole);
            allRoles.add(musicRole);
            allRoles.add(spamRole);
            allRoles.add(gamingRole);
            allRoles.add(musicCMDRole);
            allRoles.add(politicsRole);
            allRoles.add(technikRole);
            allRoles.add(serienRole);
            allRoles.add(creativeRole);
            allRoles.add(selfiesRole);
            allRoles.add(memesRole);
            allRoles.add(allRole);

            switch (args[0]) {
                default: {
                    message.getChannel().sendMessage(helpMessage).queue();
                    return CommandResponse.ACCEPTED;
                }
                case "nsfw": {
                    if (Youth.getInstance().hasRole(message.getMember(), "nsfw")) {
                        message.getGuild().getController().removeSingleRoleFromMember(message.getMember(), nsfwRole).queue();
                        message.getChannel().sendMessage(String.format(Messages.ROLE_REMOVE, "nsfw")).queue();
                        return CommandResponse.ACCEPTED;
                    }
                    message.getGuild().getController().addSingleRoleToMember(message.getMember(), nsfwRole).queue();
                    message.getChannel().sendMessage(String.format(Messages.ROLE_ADD, "nsfw")).queue();
                    return CommandResponse.ACCEPTED;
                }
                case "lgbt": {
                    if (Youth.getInstance().hasRole(message.getMember(), "LGBT+")) {
                        message.getGuild().getController().removeSingleRoleFromMember(message.getMember(), lgbtRole).queue();
                        message.getChannel().sendMessage(String.format(Messages.ROLE_REMOVE, "LGBT+")).queue();
                        return CommandResponse.ACCEPTED;
                    }
                    message.getGuild().getController().addSingleRoleToMember(message.getMember(), lgbtRole).queue();
                    message.getChannel().sendMessage(String.format(Messages.ROLE_ADD, "LGBT+")).queue();
                    return CommandResponse.ACCEPTED;
                }
                case "musik-und-videos": {
                    if (Youth.getInstance().hasRole(message.getMember(), "musik-und-videos")) {
                        message.getGuild().getController().removeSingleRoleFromMember(message.getMember(), musicRole).queue();
                        message.getChannel().sendMessage(String.format(Messages.ROLE_REMOVE, "musik-und-videos")).queue();
                        return CommandResponse.ACCEPTED;
                    }
                    message.getGuild().getController().addSingleRoleToMember(message.getMember(), musicRole).queue();
                    message.getChannel().sendMessage(String.format(Messages.ROLE_ADD, "musik-und-videos")).queue();
                    return CommandResponse.ACCEPTED;
                }
                case "spam": {
                    if (Youth.getInstance().hasRole(message.getMember(), "spam")) {
                        message.getGuild().getController().removeSingleRoleFromMember(message.getMember(), spamRole).queue();
                        message.getChannel().sendMessage(String.format(Messages.ROLE_REMOVE, "spam")).queue();
                        return CommandResponse.ACCEPTED;
                    }
                    message.getGuild().getController().addSingleRoleToMember(message.getMember(), spamRole).queue();
                    message.getChannel().sendMessage(String.format(Messages.ROLE_ADD, "spam")).queue();
                    return CommandResponse.ACCEPTED;
                }
                case "gaming": {
                    if (Youth.getInstance().hasRole(message.getMember(), "gaming")) {
                        message.getGuild().getController().removeSingleRoleFromMember(message.getMember(), gamingRole).queue();
                        message.getChannel().sendMessage(String.format(Messages.ROLE_REMOVE, "gaming")).queue();
                        return CommandResponse.ACCEPTED;
                    }
                    message.getGuild().getController().addSingleRoleToMember(message.getMember(), gamingRole).queue();
                    message.getChannel().sendMessage(String.format(Messages.ROLE_ADD, "gaming")).queue();
                    return CommandResponse.ACCEPTED;
                }
                case "musik-cmd": {
                    if (Youth.getInstance().hasRole(message.getMember(), "musik-cmd")) {
                        message.getGuild().getController().removeSingleRoleFromMember(message.getMember(), musicCMDRole).queue();
                        message.getChannel().sendMessage(String.format(Messages.ROLE_REMOVE, "musik-cmd")).queue();
                        return CommandResponse.ACCEPTED;
                    }
                    message.getGuild().getController().addSingleRoleToMember(message.getMember(), musicCMDRole).queue();
                    message.getChannel().sendMessage(String.format(Messages.ROLE_ADD, "musik-cmd")).queue();
                    return CommandResponse.ACCEPTED;
                }
                case "politik": {
                    if (Youth.getInstance().hasRole(message.getMember(), "politics")) {
                        message.getGuild().getController().removeSingleRoleFromMember(message.getMember(), politicsRole).queue();
                        message.getChannel().sendMessage(String.format(Messages.ROLE_REMOVE, "politics")).queue();
                        return CommandResponse.ACCEPTED;
                    }
                    message.getGuild().getController().addSingleRoleToMember(message.getMember(), politicsRole).queue();
                    message.getChannel().sendMessage(String.format(Messages.ROLE_ADD, "politics")).queue();
                    return CommandResponse.ACCEPTED;
                }
                case "technik": {
                    if (Youth.getInstance().hasRole(message.getMember(), "technik")) {
                        message.getGuild().getController().removeSingleRoleFromMember(message.getMember(), technikRole).queue();
                        message.getChannel().sendMessage(String.format(Messages.ROLE_REMOVE, "technik")).queue();
                        return CommandResponse.ACCEPTED;
                    }
                    message.getGuild().getController().addSingleRoleToMember(message.getMember(), technikRole).queue();
                    message.getChannel().sendMessage(String.format(Messages.ROLE_ADD, "technik")).queue();
                    return CommandResponse.ACCEPTED;
                }
                case "serien": {
                    if (Youth.getInstance().hasRole(message.getMember(), "serien")) {
                        message.getGuild().getController().removeSingleRoleFromMember(message.getMember(), serienRole).queue();
                        message.getChannel().sendMessage(String.format(Messages.ROLE_REMOVE, "serien")).queue();
                        return CommandResponse.ACCEPTED;
                    }
                    message.getGuild().getController().addSingleRoleToMember(message.getMember(), serienRole).queue();
                    message.getChannel().sendMessage(String.format(Messages.ROLE_ADD, "serien")).queue();
                    return CommandResponse.ACCEPTED;
                }
                case "design": {
                    if (Youth.getInstance().hasRole(message.getMember(), "creative")) {
                        message.getGuild().getController().removeSingleRoleFromMember(message.getMember(), creativeRole).queue();
                        message.getChannel().sendMessage(String.format(Messages.ROLE_REMOVE, "creative")).queue();
                        return CommandResponse.ACCEPTED;
                    }
                    message.getGuild().getController().addSingleRoleToMember(message.getMember(), creativeRole).queue();
                    message.getChannel().sendMessage(String.format(Messages.ROLE_ADD, "creative")).queue();
                    return CommandResponse.ACCEPTED;
                }
                case "selfies": {
                    if (Youth.getInstance().hasRole(message.getMember(), "selfies")) {
                        message.getGuild().getController().removeSingleRoleFromMember(message.getMember(), selfiesRole).queue();
                        message.getChannel().sendMessage(String.format(Messages.ROLE_REMOVE, "selfies")).queue();
                        return CommandResponse.ACCEPTED;
                    }
                    message.getGuild().getController().addSingleRoleToMember(message.getMember(), selfiesRole).queue();
                    message.getChannel().sendMessage(String.format(Messages.ROLE_ADD, "selfies")).queue();
                    return CommandResponse.ACCEPTED;
                }
                case "memes": {
                    if (Youth.getInstance().hasRole(message.getMember(), "memes")) {
                        message.getGuild().getController().removeSingleRoleFromMember(message.getMember(), memesRole).queue();
                        message.getChannel().sendMessage(String.format(Messages.ROLE_REMOVE, "memes")).queue();
                        return CommandResponse.ACCEPTED;
                    }
                    message.getGuild().getController().addSingleRoleToMember(message.getMember(), memesRole).queue();
                    message.getChannel().sendMessage(String.format(Messages.ROLE_ADD, "memes")).queue();
                    return CommandResponse.ACCEPTED;
                }
                case "kinkynsfw": {
                    if (Youth.getInstance().hasRole(message.getMember(), "kinkynsfw")) {
                        message.getGuild().getController().removeSingleRoleFromMember(message.getMember(), kinkRole).queue();
                        message.getChannel().sendMessage(String.format(Messages.ROLE_REMOVE, "kinkynsfw")).queue();
                        return CommandResponse.ACCEPTED;
                    }
                    message.getGuild().getController().addSingleRoleToMember(message.getMember(), kinkRole).queue();
                    message.getChannel().sendMessage(String.format(Messages.ROLE_ADD, "kinkynsfw")).queue();
                    return CommandResponse.ACCEPTED;
                }
                case "all": {
                    if (Youth.getInstance().hasRole(message.getMember(), "all")) {
                        message.getGuild().getController().removeRolesFromMember(message.getMember(), allRoles).complete();
                        message.getChannel().sendMessage(Messages.ALLROLES_REMOVE).queue();
                        return CommandResponse.ACCEPTED;
                    }
                    message.getGuild().getController().addRolesToMember(message.getMember(), allRoles).complete();
                    message.getChannel().sendMessage(Messages.ALLROLES_ADD).queue();
                    return CommandResponse.ACCEPTED;
                }
            }
        }
    }
}
