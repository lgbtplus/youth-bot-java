package de.lgbtplus.youth.command.commands;

import de.lgbtplus.youth.Youth;
import de.lgbtplus.youth.command.Command;
import de.lgbtplus.youth.command.CommandResponse;
import de.lgbtplus.youth.misc.Messages;
import net.dv8tion.jda.core.entities.Message;

public class SayCommand extends Command {

    public SayCommand() {
        super("say", "<text>", "Say something with the Youth Bot.");
    }

    @Override
    public CommandResponse triggerCommand(Message message, String[] args) {
        StringBuilder sayMessage = new StringBuilder();

        for (int i = 0; i < args.length; i++) {
            sayMessage.append(args[i].trim()).append(" ");
        }

        if (args.length == 0) {
            if (!Youth.getInstance().hasRole(message.getMember(), ".")) {
                message.getChannel().sendMessage(String.format(Messages.NOPERM, message.getMember().getAsMention())).complete();
                return CommandResponse.ACCEPTED;
            }
            message.getChannel().sendMessage(String.format(Messages.USAGE, "." + getCommandName() + " " + getSyntax())).complete();
            return CommandResponse.ACCEPTED;
        }
        if (Youth.getInstance().hasRole(message.getMember(), ".")) {
            message.delete().complete();
            message.getChannel().sendMessage(sayMessage).complete();
            return CommandResponse.ACCEPTED;
        }
        return CommandResponse.ACCEPTED;
    }
}
