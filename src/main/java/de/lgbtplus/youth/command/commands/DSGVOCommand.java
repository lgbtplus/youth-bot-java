package de.lgbtplus.youth.command.commands;

import de.lgbtplus.youth.Youth;
import de.lgbtplus.youth.command.Command;
import de.lgbtplus.youth.command.CommandResponse;
import de.lgbtplus.youth.misc.Messages;
import de.lgbtplus.youth.misc.YouthUser;
import net.dv8tion.jda.core.entities.Message;

public class DSGVOCommand extends Command {

    public DSGVOCommand() {
        super("dsgvo", "<accept>", "accept the GDPR/DSGVO");
    }

    @Override
    public CommandResponse triggerCommand(Message message, String[] args) {
        if(args.length < 1 || args.length > 1 || !args[0].equalsIgnoreCase("accept")) {
            message.getChannel().sendMessage(String.format(Messages.USAGE, "." + getCommandName() + " " + getSyntax())).queue();
            return CommandResponse.ACCEPTED;
        }

        YouthUser youthUser = Youth.getInstance().getUserManager().getYouthUser(message.getAuthor().getName(), message.getAuthor().getDiscriminator());
        if(youthUser == null) {
            youthUser = Youth.getInstance().getUserManager().registerUser(message.getAuthor().getName(), message.getAuthor().getDiscriminator());
        }

        youthUser.setAccepted(true);

        message.getChannel().sendMessage("Du kannst jetzt alle Commands nutzen").queue();
        Youth.getInstance().getUserManager().saveFile();

        return CommandResponse.ACCEPTED;
    }
}
