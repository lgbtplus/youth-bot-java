package de.lgbtplus.youth.command.commands;

import de.lgbtplus.youth.Youth;
import de.lgbtplus.youth.command.Command;
import de.lgbtplus.youth.command.CommandResponse;
import de.lgbtplus.youth.misc.Messages;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;

public class BanLogCommand extends Command {

    public BanLogCommand() {
        super("banlog", "<user> <reason>", "Get the Banlog");
    }

    @Override
    public CommandResponse triggerCommand(Message message, String[] args) {
        StringBuilder banReason = new StringBuilder();
        for (int i = 1; i < args.length; i++) {
            banReason.append(args[i].trim()).append(" ");
        }
        if (args.length <= 1) {
            if (!Youth.getInstance().hasRole(message.getMember(), "team")) {
                message.getChannel().sendMessage(String.format(Messages.NOPERM, message.getMember().getAsMention())).queue();
                return CommandResponse.ACCEPTED;
            }
            message.getChannel().sendMessage(String.format(Messages.USAGE, "." + getCommandName() + " " + getSyntax())).queue();
            return CommandResponse.ACCEPTED;
        }
        if (Youth.getInstance().hasRole(message.getMember(), "team")) {
            EmbedBuilder embed = new EmbedBuilder()
                    .setColor(0xc23616)
                    .setAuthor("— Neuer Bann —", "https://lgbtplus.de/t/lgbtplusde", "https://data.lgbtplus.de/branding/TwitterDesign/Profile_Picture_Colorful.png")
                    .addField("Benutzer", args[0], false)
                    .addField("Grund", banReason.toString(), false)
                    .addField("Gebannt von: ", message.getMember().getAsMention(), false);
            message.getChannel().sendMessage(embed.build()).queue();
            return CommandResponse.ACCEPTED;
        }
        return CommandResponse.ACCEPTED;
    }
}
