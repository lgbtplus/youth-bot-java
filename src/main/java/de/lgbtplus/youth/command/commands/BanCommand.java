package de.lgbtplus.youth.command.commands;

import de.lgbtplus.youth.Youth;
import de.lgbtplus.youth.command.Command;
import de.lgbtplus.youth.command.CommandResponse;
import de.lgbtplus.youth.misc.Messages;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.*;

import java.util.List;

public class BanCommand extends Command {

    public BanCommand() {
        super("ban", "<user-mention> <reason>", "You can ban a user");
    }

    @Override
    public CommandResponse triggerCommand(Message message, String[] args) {
        if (message.isFromType(ChannelType.TEXT)) {

            StringBuilder banReason = new StringBuilder();
            for (int i = 0; i < args.length; i++) {
                banReason.append(args[i].trim()).append(" ");
            }

            if (!message.getMember().hasPermission(Permission.KICK_MEMBERS) || !Youth.getInstance().hasRole(message.getMember(), "team")) {
                message.getTextChannel().sendMessage(String.format(Messages.NOPERM, message.getMember().getAsMention())).queue();
                return CommandResponse.ACCEPTED;
            }

            if (message.getMentionedUsers().isEmpty()) {
                message.getTextChannel().sendMessage("Du musst einen Benutzer markieren, um zu bannen.").queue();
                return CommandResponse.ACCEPTED;
            }


            List<User> mentionedUsers = message.getMentionedUsers();
            for (User user : mentionedUsers) {
                Member member = message.getGuild().getMember(user);

                if (!message.getMember().canInteract(member)) {
                    message.getTextChannel().sendMessage("Kann ").append(member.getEffectiveName()).append(" nicht bannen...").queue();
                    continue;
                }
                EmbedBuilder embed = new EmbedBuilder()
                        .setColor(0xc23616)
                        .setAuthor("— Neuer Bann —", "https://lgbtplus.de/t/lgbtplusde", "https://data.lgbtplus.de/branding/TwitterDesign/Profile_Picture_Colorful.png")
                        .addField("Benutzer", "" + member.getAsMention(), false)
                        .addField("Grund", banReason.toString(), false)
                        .addField("Gebannt von: ", message.getMember().getAsMention(), false);
                message.getChannel().sendMessage(embed.build()).queue();
                Youth.getInstance().getGuild().getController().ban(member, 365, banReason.toString()).queue();
                return CommandResponse.ACCEPTED;
            }
        }
        return CommandResponse.ACCEPTED;
    }

}
