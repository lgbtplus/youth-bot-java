package de.lgbtplus.youth.misc;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class YouthUser {

    @NonNull
    private String name;
    @NonNull
    private String discriminator;

    private boolean accepted;

}
