package de.lgbtplus.youth.misc;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import de.lgbtplus.youth.Youth;
import lombok.Getter;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.User;

import java.io.*;
import java.util.*;

public class UserManager {

    @Getter
    private List<YouthUser> youthUsers = new ArrayList<>();
    private Map<User, YouthUser> userRelations = new HashMap<>();

    private final File userListFile = new File("youthUsers.json");
    private final Gson gson = new Gson();

    public YouthUser getYouthUser(String name, String discriminator) {
        User member = Youth.getInstance().getJda().getUsers().stream().filter(user -> user.getName().equalsIgnoreCase(name) && user.getDiscriminator().equalsIgnoreCase(discriminator)).findFirst().orElse(null);

        if(userRelations.containsKey(member))  {
           return userRelations.get(member);
        }

        YouthUser toFind =  youthUsers.stream().filter(youthUser -> youthUser.getName().equalsIgnoreCase(name) && youthUser.getDiscriminator().equalsIgnoreCase(discriminator)).findFirst().orElse(null);
        if(toFind != null)
            userRelations.put(member, toFind);
        return toFind;
    }

    public YouthUser registerUser(String name, String discriminator) {
        YouthUser toFind =  youthUsers.stream().filter(youthUser -> youthUser.getName().equalsIgnoreCase(name) && youthUser.getDiscriminator().equalsIgnoreCase(discriminator)).findFirst().orElse(null);
        if(toFind == null) {
            toFind = new YouthUser(name, discriminator);
            youthUsers.add(toFind);
        }
        return toFind;
    }

    public void readFile() {
        if(!userListFile.exists()) {
            try {
                userListFile.createNewFile();

                try (PrintStream out = new PrintStream(new FileOutputStream(userListFile))) {
                    out.print("[]");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            youthUsers = new ArrayList<>(Arrays.asList(gson.fromJson(new JsonReader(new FileReader(userListFile)), YouthUser[].class)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    public void saveFile() {
        try {
            String json = gson.toJson(youthUsers.toArray(), YouthUser[].class);
            System.out.println(json);
            try (PrintStream out = new PrintStream(new FileOutputStream(userListFile))) {
                out.print(json);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
