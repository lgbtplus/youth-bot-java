package de.lgbtplus.youth.misc;

public class Messages {

    public static final String NOPERM = "Du hast keine Berechtigung, diesen Befehl zu nutzen %s";
    public static final String USAGE = "Syntax: %s";

    public static final String ROLE_ADD = "Du hast dir die Rolle %s hinzugefügt.";
    public static final String ROLE_REMOVE = "Du hast dir die Rolle %s entfernt";
    public static final String ALLROLES_ADD = "Du hast dir alle Rollen hinzugefügt.";
    public static final String ALLROLES_REMOVE = "Du hast dir alle Rollen entfernt";
}
