package de.lgbtplus.youth;

import de.lgbtplus.youth.command.CommandRegistry;
import de.lgbtplus.youth.command.DefaultCommandRegistry;
import de.lgbtplus.youth.command.commands.*;
import de.lgbtplus.youth.listener.OgayMessageListener;
import de.lgbtplus.youth.listener.UserJoinListener;
import de.lgbtplus.youth.listener.VoiceChannelJoinListener;
import de.lgbtplus.youth.listener.VoiceChannelLeaveListener;
import de.lgbtplus.youth.misc.UserManager;
import lombok.Getter;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.OnlineStatus;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Role;

import javax.security.auth.login.LoginException;

@Getter
public class Youth {

    @Getter
    private static Youth instance;
    @Getter
    public JDA jda;
    private final long TEST = 427733674235002880L;
    private final long PROD = 389458795237081089L;
    @Getter
    private CommandRegistry commandRegistry;
    @Getter
    private UserManager userManager;

    private Youth() {

        instance = this;
        commandRegistry = new DefaultCommandRegistry();
        try {
            jda = new JDABuilder(AccountType.BOT)
                    .setToken("NDI3NzQwMzEwMDQ3ODE3NzI5.DkZkCA.lNML7q6i3vrs2gINi8EIEdvOJL8") //TODO Change key before updating productive bot
                    .setAutoReconnect(true)
                    .setGame(Game.playing("lgbtplus.de"))
                    .setStatus(OnlineStatus.ONLINE)
                    .buildAsync();
        } catch (LoginException ex) {
            System.err.println(ex.getMessage());
        }

        userManager = new UserManager();
        userManager.readFile();

        registerCommands();
        registerEvents();

    }

    public static void main(String[] args) {
       new Youth();
    }

    private void registerEvents() {
        jda.addEventListener(commandRegistry);
        jda.addEventListener(new OgayMessageListener());
        jda.addEventListener(new VoiceChannelJoinListener());
        jda.addEventListener(new VoiceChannelLeaveListener());
        jda.addEventListener(new UserJoinListener());
    }

    private void registerCommands() {
        commandRegistry.registerCommand(new RoleCommand());
        commandRegistry.registerCommand(new RulesCommand());
        commandRegistry.registerCommand(new SayCommand());
        commandRegistry.registerCommand(new BanLogCommand());
        commandRegistry.registerCommand(new BanCommand());
        commandRegistry.registerCommand(new DSGVOCommand());

    }

    public Guild getGuild() {
        return getJda().getGuildById(PROD);
    }

    public boolean hasRole(Member member, String name) {
        for (Role roles : member.getRoles()) {
            if (roles.getName().equalsIgnoreCase(name)) {
                return true;
            }
        }
        return false;
    }



}
