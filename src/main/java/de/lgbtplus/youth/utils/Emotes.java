package de.lgbtplus.youth.utils;

import de.lgbtplus.youth.Youth;
import net.dv8tion.jda.core.entities.Emote;

import java.util.List;

public class Emotes {

    private List<Emote> emoteList;

    public Emotes(String emoteName) {
       emoteList =  Youth.getInstance().getGuild().getEmotesByName(emoteName, true);
    }

    public Emote toEmote() {
        return emoteList.get(0);
    }

}
