package de.lgbtplus.youth.listener;

import de.lgbtplus.youth.Youth;
import de.lgbtplus.youth.misc.YouthUser;
import net.dv8tion.jda.core.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class UserJoinListener extends ListenerAdapter {

    @Override
    public void onGuildMemberJoin(GuildMemberJoinEvent event) {
        YouthUser youthUser = Youth.getInstance().getUserManager().getYouthUser(event.getUser().getName(), event.getUser().getDiscriminator());
        if(youthUser == null) {
            event.getUser().openPrivateChannel().queue(privateChannel -> {
                privateChannel.sendMessage("Hey \uD83D\uDC4B Es scheint als bist du neu auf unserem Server. Herzlich Willkommen \uD83D\uDE42 \nWenn du unsere Commands auf dem Server nutzen " +
                        "willst, musst du zustimmen, dass wir personenbezogene Daten von dir speichern dürfen. Bitte gibt daher auf dem Server einmal den folgenden Befehl ein: \n" +
                        "**.dsgvo accept**").queue();
            });
            return;
        }
        if(!youthUser.isAccepted()) {
            event.getUser().openPrivateChannel().queue(privateChannel -> {
                privateChannel.sendMessage("Hey \uD83D\uDC4B Es scheint als bist du neu auf unserem Server. Herzlich Willkommen \uD83D\uDE42 \nWenn du unsere Commands auf dem Server nutzen " +
                        "willst, musst du zustimmen, dass wir personenbezogene Daten von dir speichern dürfen. Bitte gibt daher auf dem Server einmal den folgenden Befehl ein: \n" +
                        "**.dsgvo accept**").queue();
            });
        }
    }

}
