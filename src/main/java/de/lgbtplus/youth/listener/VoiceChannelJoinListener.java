package de.lgbtplus.youth.listener;

import de.lgbtplus.youth.Youth;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.events.guild.voice.GuildVoiceJoinEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class VoiceChannelJoinListener extends ListenerAdapter {

    @Override
    public void onGuildVoiceJoin(GuildVoiceJoinEvent event) {
        TextChannel voiceTextChannel = event.getGuild().getTextChannelsByName("voice", true).get(0);
        voiceTextChannel.putPermissionOverride(event.getMember()).setAllow(Permission.VIEW_CHANNEL).queue();
    }
}
