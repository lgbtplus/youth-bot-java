package de.lgbtplus.youth.listener;

import de.lgbtplus.youth.utils.Emotes;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class OgayMessageListener extends ListenerAdapter {

    public void onMessageReceived(MessageReceivedEvent event) {
        if (event.getMessage().getContentRaw().toLowerCase().contains("ogay") && !event.getAuthor().isBot()) {
            event.getMessage().addReaction(new Emotes("pridesinger").toEmote()).queue();
            event.getChannel().sendMessage(event.getAuthor().getAsMention() + ", OGAYYY!!!! " + new Emotes("pridesinger").toEmote().getAsMention()).queue();
        }
    }

}
