package de.lgbtplus.youth.listener;

import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.events.guild.voice.GuildVoiceLeaveEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class VoiceChannelLeaveListener extends ListenerAdapter {

    @Override
    public void onGuildVoiceLeave(GuildVoiceLeaveEvent event) {
        TextChannel voiceTextChannel = event.getGuild().getTextChannelsByName("voice", true).get(0);
        voiceTextChannel.getPermissionOverride(event.getMember()).delete().queue();
    }
}
