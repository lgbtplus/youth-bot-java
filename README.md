# [Discord Bot](https://lgbtplus.de) MASTER BRANCH
[![Version](https://img.shields.io/badge/Version-1.1.3-blue.svg?style=flat-square)](https://lgptplus.de) [![Version](https://discordapp.com/api/guilds/389458795237081089/widget.png)](https://lgbtplus.de)
The official Discord-Bot of the lgbtplus.de Server.

## Content

- [Changelog](#changelog)
- [Authors](#authors)
- [Copyright & License](#copyright-&-license)


## Versions & Changelog
OUTDATED

youth.java-1.1.2

# Changelog
The actual notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.1.3] - 08.08.2018
### Added
- GDPR user management
- gdpr command

### Changed
 - little bugfixes
 - command block in command registry

## [1.1.2] - 04.08.2018
### Added
- Survey command

### Changed
 - Dynamic syntax message
  
## [1.1.1] - 29.07.2018
### Changed
 - Rules- and Banlog command (NoPerm)

## [1.1.0] - 29.07.2018
### Added
 - Command Manager
 - Messages class
 - Lombok (IDE Plugin needed)
 - Gitlab CI
 - Git Ignore

### Changed
 - build plugins in pom.xml
 - Command optimization
 - RAM optimization
 
### Removed
 - unused dependencies

## [1.0.0] - 25.07.2018
### Added
  - Text-Voicechannel
  - Kick, Ban, BanLog, Role, Rules & Say Command


## Authors

**dieserjxhn**
- <https://twitter.com/dieserjxhn> [GERMAN]
- <https://lgbtplus.de/go/discord> [GERMAN]


## Copyright & License

This project is licensed under [the MIT-License.](https://gitlab.com/lgbtplus/youth-bot/blob/master/LICENSE).
